package main

import (
	"flag"
	"fmt"
	"github.com/mxk/go-sqlite/sqlite3"
	"github.com/nfnt/resize"
	"image"
	"image/jpeg"
	"image/png"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"time"
)

var (
	targetPath string
	total      int
	current    int
	names      []string
)

func main() {

	delay := flag.Int64("delay", 1000, "delay in milliseconds between requests to google")
	dbname := flag.String("db", "database.db", "database to read from")
	gui := flag.Bool("gui", false, "use gui to choose image")
	flag.StringVar(&targetPath, "target", "images", "where to store image")
	flag.Parse()
	DB, err := PathToABS(*dbname)
	if err != nil {
		log.Fatal(err)
	}
	targetPath, err = PathToABS(targetPath)
	if err != nil {
		log.Fatal(err)
	}
	db, err := sqlite3.Open(DB)
	if err != nil {
		log.Fatal(err)
	}

	s, err := db.Query("SELECT name from items")
	if err != nil {
		log.Fatal(err)
	}

	for err == nil {
		str := ""
		s.Scan(&str)
		str = str[:len(str)-4]
		if !haveImage(str) {
			names = append(names, str)
		}
		err = s.Next()
	}

	total = len(names)

	if *gui {
		var httpd Httpd
		httpd.run()

	} else {

		for index, str := range names {
			fmt.Printf("\033[0;32m%d/%d |\033[0m", index, total)
			doRequest(str)
			time.Sleep(time.Duration(int64(*delay)) * time.Millisecond)
		}
	}

	s.Close()

}

// Search google for images
func Search(name string) []string {
	url := "https://www.google.se/search?hl=sv&biw=1283&bih=86&tbm=isch&sa=1&q=" + name + "+mame"
	client := &http.Client{}
	req, err := http.NewRequest("GET", url, nil)
	req.Header.Add("user-agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36")
	response, err := client.Do(req)
	if err != nil {
		log.Print(err)
		return []string{}
	}

	defer response.Body.Close()

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Print(err)
		return []string{}
	}

	re := regexp.MustCompile("\"ou\":\"(http://|https://)([^:]*)(.png|.jpg)\"")
	links := re.FindAllSubmatch(body, 10)
	if len(links) == 0 {

		fmt.Println("\033[0;31m Can not find any image (" + name + ") \033[0m")
		return []string{}
	}
	var images []string
	for _, base := range links {
		images = append(images, string(base[1])+string(base[2])+string(base[3]))
	}

	return images

}

func doRequest(name string) {
	url := "https://www.google.se/search?hl=sv&biw=1283&bih=86&tbm=isch&sa=1&q=" + name + "+mame"
	client := &http.Client{}
	req, err := http.NewRequest("GET", url, nil)
	req.Header.Add("user-agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36")
	response, err := client.Do(req)
	if err != nil {
		log.Print(err)
		return
	}

	defer response.Body.Close()

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Print(err)
		return
	}
	re := regexp.MustCompile("\"ou\":\"(http://|https://)([^:]*)(.png|.jpg)\"")
	links := re.FindAllSubmatch(body, 30)
	if len(links) == 0 {
		fmt.Println("\033[0;31m Can not find any image (" + name + ") \033[0m")
		return
	}
	l := links[0]
	imgURL := string(l[1]) + string(l[2]) + string(l[3])
	err = createImageFile(imgURL, name+string(l[3]))
	if err != nil {
		fmt.Println("\033[0;31m " + err.Error() + "\033[0m")
	} else {
		fmt.Println("\033[0;35m created image " + name + string(l[3]) + "\033[0m")
	}

}

func haveImage(str string) bool {
	if _, err := os.Stat(targetPath + "/" + str + ".png"); !os.IsNotExist(err) {
		return true
	}

	if _, err := os.Stat(targetPath + "/" + str + ".jpg"); !os.IsNotExist(err) {
		return true
	}
	return false
}

func currentExtension(name string) string {
	if _, err := os.Stat(targetPath + "/" + name + ".png"); !os.IsNotExist(err) {
		return ".png"
	}

	if _, err := os.Stat(targetPath + "/" + name + ".jpg"); !os.IsNotExist(err) {
		return ".jpg"
	}
	return ""
}

func createImageFile(url, name string) error {
	response, err := http.Get(url)
	if err != nil {
		return err
	}
	defer response.Body.Close()

	image, _, err := image.Decode(response.Body)
	if err != nil {
		return err
	}
	newImage := resize.Resize(240, 0, image, resize.Lanczos3)
	file, err := os.Create(targetPath + "/" + name)
	if err != nil {
		return err
	}

	defer file.Close()

	if strings.HasSuffix(name, ".jpg") {
		jpeg.Encode(file, newImage, &jpeg.Options{jpeg.DefaultQuality})
	}
	if strings.HasSuffix(name, ".png") {
		png.Encode(file, newImage)
	}
	return nil
}

// PathToABS givbe full path back
func PathToABS(inPath string) (string, error) {
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		return inPath, err
	}
	if !strings.HasPrefix(inPath, "/") {
		inPath = dir + "/" + inPath
	}
	return inPath, nil
}
