package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"
)

// Httpd Server for gui view
type Httpd struct {
}

func (httpd *Httpd) run() {
	http.HandleFunc("/", httpd.handleRequest)
	http.HandleFunc("/search", httpd.handleSearch)
	http.HandleFunc("/save", httpd.handleSave)
	http.ListenAndServe(":8101", nil)
}

func (httpd *Httpd) handleSearch(w http.ResponseWriter, r *http.Request) {
	index, err := strconv.Atoi(r.FormValue("index"))
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		log.Println(err)
		return
	}

	if index < 0 || index >= total {

		http.Error(w, "Index out of range", http.StatusBadRequest)
		log.Println("Index out of range")
		return
	}

	str := names[index]
	images := Search(str)
	var wr WebResponse
	wr.Name = str
	wr.Images = images
	wr.Total = total
	b, err := json.Marshal(wr)
	if err != nil {
		httpd.internalError(err.Error(), w)
		log.Println(err)
		return
	}
	fmt.Fprint(w, string(b))
	current++
}

func (httpd *Httpd) handleSave(w http.ResponseWriter, r *http.Request) {
	name := r.FormValue("name")
	image := r.FormValue("image")
	extension := ""
	if name == "" || image == "" {
		http.Error(w, "Missing name and/or image url", http.StatusBadRequest)
		return
	}

	if strings.HasSuffix(image, ".png") {
		extension = ".png"
	}

	if strings.HasSuffix(image, ".jpg") {
		extension = ".jpg"
	}

	if extension == "" {
		http.Error(w, "Unsupported type", http.StatusUnsupportedMediaType)
		return
	}
	createImageFile(image, name+extension)
}

func (httpd *Httpd) handleRequest(w http.ResponseWriter, r *http.Request) {

	if r.RequestURI == "/" {
		p, err := PathToABS("assets/html/index.html")
		if httpd.notFound(err, w, r) {
			return
		}
		http.ServeFile(w, r, p)
		return
	}

	if strings.HasPrefix(r.RequestURI, "/image/") {
		img := strings.TrimPrefix(r.RequestURI, "/image/")

		if !haveImage(img) {
			p, err := PathToABS("assets/images/default_fanout.png")
			if httpd.notFound(err, w, r) {
				return
			}
			http.ServeFile(w, r, p)
			return
		}

		p, err := PathToABS(targetPath + img)
		if httpd.notFound(err, w, r) {
			return
		}
		http.ServeFile(w, r, p)
		return
	}

	p, err := PathToABS("assets/" + strings.TrimLeft(r.RequestURI, "/"))
	if httpd.notFound(err, w, r) {
		return
	}
	http.ServeFile(w, r, p)

}

func (httpd *Httpd) notFound(err error, w http.ResponseWriter, r *http.Request) bool {
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		log.Println(err)
		return true
	}
	return false
}

func (httpd *Httpd) internalError(message string, w http.ResponseWriter) {
	http.Error(w, message, http.StatusInternalServerError)
}

//WebResponse Response to next
type WebResponse struct {
	Total  int      `json:"total"`
	Name   string   `json:"name"`
	Images []string `json:"images"`
}
