Scanner = function () {
  this.evh = new dd.events.EventHandler();
  this.index = 0;
  this.base();
}

Scanner.prototype.base = function() {
  this.startBtn = dd.dom.createElement('button', {'class':'big-green'}, 'Starta Scanning')
  this.error = dd.dom.createElement("div",{class:'error'})
  this.container = dd.dom.createElement('div',{class:'next-container'})
  dd.dom.append(this.error, document.body)
  dd.dom.append(this.startBtn,document.body)
  dd.dom.append(this.container,document.body)
  this.total = 0;
  this.evh.listen(this.startBtn,'click',this.start,this)
}

Scanner.prototype.start = function () {
  dd.dom.addClass(this.startBtn,'hide')
  this.search();
}

Scanner.prototype.search = function () {
  var req = new dd.net.XhrIo('/search', 'POST', "index="+this.index, this);
  req.onComplete = function (e) {
      this.renderResult(e.responseJson);
  }
  req.onError = function (e) {
      this.log(e.status, e.text);
  }
  req.setRequestHeader({'Content-type': 'application/x-www-form-urlencoded'});
  req.send();
}

Scanner.prototype.back = function () {
  if (this.index > 0 ){
    this.index--
    this.search()
  }

}

Scanner.prototype.next = function () {
  if (this.index < this.total ){
    this.index++
    this.search()
  }
}

Scanner.prototype.renderResult = function(data) {
  this.evh.removeAll();
  this.total = data.total
  dd.dom.removeChildren(this.container)
  var next = dd.dom.createElement('button',{},'Next');
  var previous = dd.dom.createElement('button',{},'Previous');
  dom = dd.dom.createElement('div',{class:'container'},[
    dd.dom.createElement('div',{}, this.index+"/"+data.total),
    dd.dom.createElement('h3',{},[
      data.name,
      dd.dom.createElement('div',{class:'left'},previous),
      dd.dom.createElement('div',{class:'right'},next)
    ])
  ]);

  if (data.images.length==0){
    dd.dom.append(dd.dom.createElement('h5',{},'No images found...'), dom)
  }else{
    dd.array.each(data.images,function(img) {
      var image =  dd.dom.createElement('img',{src:img, width: '230',height: '180'})
      this.evh.listen(image,'click',function() {
        dd.dom.addClass(image,'selected')
        this.save(data.name,image.src)
      },this)
      dd.dom.append(image, dom)
    },this);
    
  }
  dd.dom.append(dom, this.container)
  this.evh.listen(next,'click',this.next,this)
  this.evh.listen(previous,'click',this.back,this)
}

Scanner.prototype.save = function(name, image) {
 var req = new dd.net.XhrIo('/save', 'POST', "name="+name+"&image="+image, this);
    req.onComplete = function (e) {
      this.next();
    }
    req.onError = function (e) {
        this.log(e.status, e.text);
    }
  req.setRequestHeader({'Content-type': 'application/x-www-form-urlencoded'});
  req.send();
}

Scanner.prototype.log = function(status, message) {
  var dom = dd.dom.createElement('div',{class:'error-message'}, [
    dd.dom.createElement('b',{},""+status),
    message
  ]);
  dd.dom.append(dom, this.error)
  setTimeout(function(){dd.dom.remove(dom);},5000);
}